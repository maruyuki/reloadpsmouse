#!/bin/sh

echo "[Unit]
Description=Unload psmouse
Before=suspend.target

[Service]
Type=simple
ExecStart=/sbin/modprobe -r psmouse

[Install]
WantedBy=suspend.target" > /etc/systemd/system/reloadpsmouse-suspend.service

echo "[Unit]
Description=Load psmouse
After=suspend.target

[Service]
Type=simple
ExecStart=/sbin/modprobe psmouse

[Install]
WantedBy=suspend.target" > /etc/systemd/system/reloadpsmouse-resume.service

systemctl daemon-reload
systemctl enable reloadpsmouse-suspend.service
systemctl enable reloadpsmouse-resume.service